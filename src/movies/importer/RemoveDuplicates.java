package movies.importer;
import java.util.ArrayList;

public class RemoveDuplicates extends Processor{
	public RemoveDuplicates(String srcDir, String destDir) {
		super(srcDir,destDir,false);
	}
	
	public ArrayList <String> process(ArrayList <String> strarr){
		ArrayList<String> noDuplicate = strarr;
		//q, a, b, a, c

			for(int j=0;j<strarr.size();j++) {
				if(noDuplicate.contains(strarr.get(j))) {
					noDuplicate.remove(strarr.get(j));
				}
			}
		
		return noDuplicate;
	}
}
