package movies.importer;
import java.util.ArrayList;

public class LowercaseProcessor extends Processor{
	public LowercaseProcessor(String srcDir, String destDir) {
		super(srcDir,destDir,true);
	}
	
	public ArrayList <String> process(ArrayList <String> strarr){
		ArrayList<String> asLower = new ArrayList<String>();
		for(int i=0;i<strarr.size();i++) {
			asLower.add(strarr.get(i).toLowerCase());
		}
		return asLower;
	}
}
