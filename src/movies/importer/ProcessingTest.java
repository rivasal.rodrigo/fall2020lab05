package movies.importer;
import java.io.IOException;

public class ProcessingTest {
	public static void main(String[] args) throws IOException {
		//C:\Users\Rodri\Documents\programming3\lab5
		String source = "C:\\Users\\Rodri\\Documents\\programming3\\lab5";
		String dest = "C:\\Users\\Rodri\\Documents\\programming3\\lab5\\LowercaseProcessed";
		LowercaseProcessor lwp = new LowercaseProcessor(source,dest);
		lwp.execute();
		
		String newSource = "C:\\Users\\Rodri\\Documents\\programming3\\lab5\\LowercaseProcessed";
		String newDest = "C:\\Users\\Rodri\\Documents\\programming3\\lab5\\RemoveDuplicates";
		RemoveDuplicates rmvd = new RemoveDuplicates(newSource,newDest);
		rmvd.execute();
	}
}
